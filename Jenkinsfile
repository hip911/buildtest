pipeline {
  agent none
  stages {
    stage('Build with Kaniko') {
      agent {
        kubernetes {
          label 'kaniko-build'
          yaml """
    kind: Pod
    metadata:
      name: kaniko
    spec:
      containers:
      - name: jnlp
        workingDir: /tmp/jenkins
        imagePullSecrets:
          - name: dockerhub
      - name: kaniko
        imagePullSecrets:
          - name: dockerhub
        workingDir: /tmp/jenkins
        image: gcr.io/kaniko-project/executor:debug
        imagePullPolicy: Always
        command:
        - /busybox/cat
        tty: true
        volumeMounts:
          - name: jenkins-docker-cfg
            mountPath: /kaniko/.docker
      volumes:
      - name: jenkins-docker-cfg
        projected:
          sources:
          - secret:
              name: dockerhub
              items:
                - key: .dockerconfigjson
                  path: config.json
    """
        }
      }
      environment {
        PATH = "/busybox:/kaniko:$PATH"
        REPO = "hip911/buildtest"
      }
      steps {
        container(name: 'kaniko', shell: '/busybox/sh') {
          sh '''#!/busybox/sh
            /kaniko/executor --context `pwd` --verbosity debug --destination $REPO:$BUILD_NUMBER
          '''
        }
      }
    }
    stage('Test Helm') {
      agent {
        kubernetes {
          label 'helm-deploy'
          yaml """
    kind: Pod
    metadata:
      name: kaniko
    spec:
      containers:
      - name: jnlp
        workingDir: /tmp/jenkins
        imagePullSecrets:
          - name: dockerhub
      - name: helm
        imagePullSecrets:
          - name: dockerhub
        workingDir: /tmp/jenkins
        image: alpine/helm
        imagePullPolicy: Always
        command: ["/bin/sh"]
        tty: true
        volumeMounts:
          - name: jenkins-docker-cfg
            mountPath: /kaniko/.docker
      volumes:
      - name: jenkins-docker-cfg
        projected:
          sources:
          - secret:
              name: dockerhub
              items:
                - key: .dockerconfigjson
                  path: config.json
      serviceAccountName: jenkins-cluster-admin
    """
        }
      }
      environment {
        PATH = "/busybox:/kaniko:$PATH"
        REPO = "hip911/buildtest"
      }
      steps {
        container(name: 'helm', shell: '/bin/sh') {
          writeFile file: "values.yaml", text: """
            image:
                repository: $REPO
                tag: $BUILD_NUMBER
            ingress:
              enabled: true
              hosts:
                - host: localhost
                  paths:
                    - path: /sample
          """
          sh '''#!/bin/sh
            helm upgrade -i -n sample-app -f values.yaml sample-app chart
          '''
        }
      }
    }
  }
}
